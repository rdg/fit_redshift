from math import log10, pow
import re

import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline, UnivariateSpline, interp1d
from scipy.optimize import fmin

import astro_utils.table_utils as tabu
import astro_utils.spectra_utils as specu


def fit_sed(params, observation, template):
    """
    Return sum of sqs of log10 diff between ``observation`` and ``template``
    """
    z, scale = params
    diff = 0
    # Basic sum of squares of differences between observed points and template
    for dispersion in observation:
        #print(dispersion / (1 + z), template(dispersion / (1 + z)))
        diff += (log10(observation[dispersion]) -
                 scale * log10(template(dispersion / (1 + z)))) ** 2
    return diff


def main():
    target_name = 'HATLAS-SGP-SG.v1.77'

    # Get observed photometry points
    targets = tabu.to_table('targets.cat')
    target = targets[targets['Name'] == target_name]
    observation = {}
    # Fill observation with ``dispersion: flux`` elements
    for colname in target.colnames:
        if (re.match('S[0-9]+', colname) is not None
                and target[colname] != '' and colname != 'S1.4'):
            observation[int(colname[1:])] = np.float64(target[colname])

    # Get template spectrum
    #template_table = tabu.to_table('smg_sed.txt')
    template_table = tabu.to_table('arp220_sed.txt')
    template = interp1d(template_table['Wave'], template_table['Flux'])
    #template = InterpolatedUnivariateSpline(template_table['Wave'],
    #                                        template_table['Flux'])

    # Define the initial scale guess as how much the template flux needs to be
    # scaled by at z=2 to have the same 350um flux as the observation
    initial_scale = log10(observation[350]) / log10(template(350. / 3))
    
    # Find the best fit to redshift and scale
    fitted_z, fitted_scale = fmin(fit_sed, (2., initial_scale),
                                  args=(observation, template) )
    print("fitted_z =", fitted_z, "   fitted_scale =", fitted_scale)

    # Plot the result
    plt.loglog(template_table['Wave'], template_table['Flux'])
    observation = [(dispersion / (1 + fitted_z),
                    pow(observation[dispersion], 1 / fitted_scale))
                   for dispersion in observation                   ]
    observation.sort()
    plt.loglog([i[0] for i in observation], [i[1] for i in observation],
               color='red', linewidth=2                                 )
    plt.show()

if __name__ == '__main__':
    main()
