from itertools import combinations
import sys

import numpy as np
from scipy.optimize import fmin

import astro_utils.spectra_utils as specu


def format_num(num, precision=2):
    """
    Format a float according to given precision, returning a string
    
    Parameters
    ----------
    num : object
        Formatted to precision if a float
    precision : int, optional
        The number of decimal places with which to format floats
        
    Returns
    -------
    str :
        ``str(num)`` formatted to ``precision`` d.p. if ``num`` is a float
    """
    if type(num) in (float, np.float64):
        return '{0:.{1}f}'.format(num, precision)
    else:
        return str(num)


def pprint_table(out, table, precision=2):
    """
    Writes a table of data to an output stream, padded for alignment
    
    Each row must have the same number of columns
    Could add another optional parameter with alignments
    
    Parameters
    ----------
    out : Output stream
        (file-like object)
    table : List of lists
        The table to print.
    precision : int, optional
        The number of decimal places with which to represent floats
    """
    # Find the widths of the longest string in each column
    col_widths = [max([len(format_num(row[i], precision)) for row in table])
                  for i in range(len(table[0]))                             ]
    # Format and print to the output stream
    for row in table:
        # The first column
        out.write('{0:>{1}}'.format(format_num(row[0]),
                                    col_widths[0]      ))
        # Middle columns (insert 2 spaces before column)
        for i in range(1, 1+len(col_widths[1:-1])):
            out.write('{0:>{1}}'.format(format_num(row[i]),
                                        col_widths[i] + 2  ))
        # Final column (left justified)
        out.write('  {0}\n'.format(row[-1]))


def fit_spectral_lines(observed_lines, dispersion_format='um'):
    """
    Fit some possible observed spectral lines to find a redshift
    
    Parameters
    ----------
    observed_lines : array like
        A list/array of dispersion values of the observed lines
    dispersion_format : str, optional
        The format of the line values``'um'`` or ``'ghz'``
    """
    template_lines = np.rec.fromrecords([('[OI]l' , 63.183705 ),
                                         ('[OIII]', 88.356    ),
                                         #('OH',     119.3     ),
                                         ('[NII]l', 121.89806 ),
                                         ('[SiI]',  129.68173 ),
                                         ('[OI]h',  145.525439),
                                         ('[CII]',  157.7409  ),
                                         ('[NII]h', 205.1782  ) ],
                                        names=('name', 'wave')    )
    
    # Convert lines to um if necessary
    obs_lines = np.asarray(observed_lines)
    obs_lines.sort()
    if dispersion_format is 'ghz':
        obs_lines = specu.um_ghz_um(obs_lines)

    # Create an array of the redshifts necessary for each observed line to
    # correspond to each template line
    redshifts = np.zeros((len(obs_lines), len(template_lines)))
    for i in range(len(obs_lines)):
        redshifts[i] = np.array([obs_lines[i]] * len(template_lines)) \
                                                / template_lines['wave'] - 1

    # Now need to find which elements are similar
    min_diffs = []
    for i in range(len(obs_lines)):
        for j in range(len(redshifts[i][redshifts[i] > 0.7])):
             copy_redshifts = redshifts.copy()
             copy_redshifts[i][j] = np.inf
             min_diffs.append((abs(copy_redshifts - redshifts[i][j]).min(),
                               (redshifts[i][redshifts[i] > 0.7][j])       ))

    # Get details of the most similar elements
    min_diff_index = np.argmin([i[0] for i in min_diffs])
    z_plus_one = min_diffs[min_diff_index][1] + 1

    # Print the result
    print(min_diffs[min_diff_index][1], min_diffs[min_diff_index][0])
    line_table = []
    line_format = 'Line (um)'
    if dispersion_format is 'ghz':
        line_format = 'Line (GHz)'
    line_table.append([line_format, 'RF wavelength', 'Name of nearest line'])
    for line in obs_lines:
        name = template_lines['name'][abs(template_lines['wave']
                                          - (line / z_plus_one) ).argmin()]
        line_table.append([specu.um_ghz_um(line), line / z_plus_one, name])
    pprint_table(sys.stdout, line_table)


def main():
    """
    Input the lines and call fit_spectral_lines
    """
    #observed_lines = [1398, 1272, 1168, 987]
    #observed_lines = [1437, 1368, 1272, 967, 1523]
    observed_lines = [582.6, 1389, 970]
    #observed_lines = [214.4, 235.7]
    fit_spectral_lines(observed_lines, dispersion_format='ghz')


if __name__ == '__main__':
    main()
